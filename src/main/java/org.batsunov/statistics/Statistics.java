package org.batsunov.statistics;


import org.batsunov.dao.BookDAO;
import org.batsunov.dao.LibraryUserDAO;

public class Statistics {


    private static BookDAO bookDAO = new BookDAO();
    private static LibraryUserDAO libraryUserDAO = new LibraryUserDAO();


    public static int numberOfAllUsers() {

        return libraryUserDAO.getAll().size();
    }

    public static int numberOfAllBook() {

        return bookDAO.getAll().size();
    }

    public static int getAvailableBook() {

        return bookDAO.getAvailable().size();
    }

    public static int getNumberBooksInUse() {

        return bookDAO.getAll().size() - bookDAO.getAvailable().size();
    }

}
