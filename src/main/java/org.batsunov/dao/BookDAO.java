package org.batsunov.dao;

import org.batsunov.Hibernate.EntityManagerUtil;
import org.batsunov.Library.UserAction;
import org.batsunov.domain.Book;
import org.batsunov.domain.HistoryEntry;
import org.batsunov.domain.LibraryUser;


import javax.persistence.*;
import java.util.List;
import java.util.Optional;

public class BookDAO {

    private final EntityManager entityManager = EntityManagerUtil.getEntityManager();

    public void save(Book entity) {

        boolean inTransaction = entityManager.isJoinedToTransaction();
        EntityTransaction transaction = entityManager.getTransaction();
        if (!inTransaction) {
            transaction.begin();
        }

        entityManager.persist(entity);

        if (!inTransaction) {
            transaction.commit();
        }
    }


    public void takeBook(Book book, LibraryUser libraryUser) {

        book.setLibraryUser(libraryUser);

        HistoryEntryDAO historyEntryDAO = new HistoryEntryDAO();
        HistoryEntry historyEntry = new HistoryEntry();
        historyEntry.setBook(book);
        historyEntry.setUser(libraryUser);
        historyEntry.setUserAction(String.valueOf(UserAction.TAKE));
        historyEntryDAO.save(historyEntry);

    }

    public void returnBook(Book book) {

        HistoryEntry historyEntry = new HistoryEntry();
        historyEntry.setBook(book);
        historyEntry.setUserAction(String.valueOf(UserAction.RETURN));
        historyEntry.setUser(book.getLibraryUser());

        HistoryEntryDAO historyEntryDAO = new HistoryEntryDAO();
        historyEntryDAO.save(historyEntry);

        book.setLibraryUser(null);

        update(book);
    }

    public List<Book> getAll() {

        Query managerQuery = entityManager.createQuery("Select u From Book u");

        return managerQuery.getResultList();
    }

    public void update(Book book) {

        boolean inTransaction = entityManager.isJoinedToTransaction();
        EntityTransaction transaction = entityManager.getTransaction();
        if (!inTransaction) {
            transaction.begin();
        }

        entityManager.merge(book);

        if (!inTransaction) {
            transaction.commit();
        }
    }

    public Optional<Book> findById(long id) {

        Query managerQuery = entityManager.createQuery("SELECT u FROM Book u where u.id = :id");
        managerQuery.setParameter("id", id);

        try {
            return Optional.of((Book) managerQuery.getSingleResult());
        } catch (NoResultException | NonUniqueResultException exc) {
            return Optional.empty();
        }
    }

    public void deleteById(long id) {

        boolean inTransaction = entityManager.isJoinedToTransaction();
        EntityTransaction transaction = entityManager.getTransaction();
        if (!inTransaction) {
            transaction.begin();
        }

        entityManager.remove(entityManager.find(Book.class, id));

        if (!inTransaction) {
            transaction.commit();
        }
    }


    public List<Book> getAvailable() {

        Query managerQuery = entityManager.createQuery("Select u From Book u where u.libraryUser = null");

        return managerQuery.getResultList();

    }
}
