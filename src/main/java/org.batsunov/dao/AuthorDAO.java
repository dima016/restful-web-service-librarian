package org.batsunov.dao;

import org.batsunov.Hibernate.EntityManagerUtil;
import org.batsunov.domain.Author;


import javax.persistence.*;
import java.util.List;
import java.util.Optional;

public class AuthorDAO {

    private final EntityManager entityManager = EntityManagerUtil.getEntityManager();

    public void save(Author entity) {

        boolean inTransaction = entityManager.isJoinedToTransaction();
        EntityTransaction transaction = entityManager.getTransaction();
        if (!inTransaction) {
            transaction.begin();
        }

        entityManager.persist(entity);

        if (!inTransaction) {
            transaction.commit();
        }
    }

    public List<Author> getAll() {

        Query managerQuery = entityManager.createQuery("SELECT u FROM Author u");


        return managerQuery.getResultList();
    }


    public Optional<Author> findByPseudonym(String pseudonym) {


        Query managerQuery = entityManager.createQuery("SELECT u FROM Author u where u.pseudonym = :pseudonym");

        managerQuery.setParameter("pseudonym", pseudonym);


        try {
            return Optional.of((Author) managerQuery.getSingleResult());
        } catch (NoResultException | NonUniqueResultException exc) {

            return Optional.empty();
        }

    }

    public Optional<Author> findById(long id) {

        Query managerQuery = entityManager.createQuery("SELECT u FROM Author u where u.id = :id");
        managerQuery.setParameter("id", id);

        try {
            return Optional.of((Author) managerQuery.getSingleResult());
        } catch (NoResultException | NonUniqueResultException exc) {
            return Optional.empty();
        }

    }


    public void deleteById(long id) {

        boolean inTransaction = entityManager.isJoinedToTransaction();
        EntityTransaction transaction = entityManager.getTransaction();
        if (!inTransaction) {
            transaction.begin();
        }

        entityManager.remove(entityManager.find(Author.class, id));

        if (!inTransaction) {
            transaction.commit();
        }
    }

}
