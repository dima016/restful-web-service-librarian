package org.batsunov.dao;

import org.batsunov.Hibernate.EntityManagerUtil;
import org.batsunov.domain.Book;
import org.batsunov.domain.HistoryEntry;
import org.batsunov.domain.LibraryUser;

import javax.persistence.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class HistoryEntryDAO {

    private final EntityManager entityManager = EntityManagerUtil.getEntityManager();

    public List<HistoryEntry> getAll() {

        Query managerQuery = entityManager.createQuery("Select u From HistoryEntry u");

        return managerQuery.getResultList();
    }

    public List<HistoryEntry> getHistoryByBook(Book book) {

        Query managerQuery = entityManager.createQuery("Select u From HistoryEntry u where u.book = :book");

        managerQuery.setParameter("book", book);

        try {
            return managerQuery.getResultList();
        } catch (NoResultException | NonUniqueResultException exc) {
            return null;
        }
    }

    public List<HistoryEntry> getHistoryByUser(LibraryUser libraryUser) {

        Query managerQuery = entityManager.createQuery("Select u From HistoryEntry u where u.user = :user");

        managerQuery.setParameter("user", libraryUser);


        try {
            return managerQuery.getResultList();
        } catch (NoResultException | NonUniqueResultException exc) {
            return null;
        }
    }


    public void save(HistoryEntry entity) {

        boolean inTransaction = entityManager.isJoinedToTransaction();
        EntityTransaction transaction = entityManager.getTransaction();
        if (!inTransaction) {
            transaction.begin();
        }

        entityManager.persist(entity);

        if (!inTransaction) {
            transaction.commit();
        }
    }
}
