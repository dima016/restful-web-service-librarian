package org.batsunov.dao;

import org.batsunov.Hibernate.EntityManagerUtil;
import org.batsunov.domain.Book;
import org.batsunov.domain.LibraryUser;

import javax.persistence.*;
import java.util.List;
import java.util.Optional;

public class LibraryUserDAO {

    private final EntityManager entityManager = EntityManagerUtil.getEntityManager();


    public void save(LibraryUser entity) {

        boolean inTransaction = entityManager.isJoinedToTransaction();
        EntityTransaction transaction = entityManager.getTransaction();
        if (!inTransaction) {
            transaction.begin();
        }

        entityManager.persist(entity);

        if (!inTransaction) {
            transaction.commit();
        }
    }


    public List<LibraryUser> getAll() {

        Query managerQuery = entityManager.createQuery("Select u From LibraryUser u");

        return managerQuery.getResultList();
    }


    public Optional<LibraryUser> findByName(String name) {


        Query managerQuery = entityManager.createQuery("SELECT u FROM LibraryUser u where u.name = :name");

        managerQuery.setParameter("name", name);

        try {
            return Optional.of((LibraryUser) managerQuery.getSingleResult());
        } catch (NoResultException | NonUniqueResultException exc) {
            return Optional.empty();
        }
    }

    public List<Book> getDebtById(long id) {

        LibraryUser libraryUser = findById(id).get();
        Query managerQuery = entityManager.createQuery("SELECT u FROM Book u where u.libraryUser = :user");
        managerQuery.setParameter("user", libraryUser);

        try {
            return managerQuery.getResultList();
        } catch (NoResultException exc) {
            return null;
        }

    }

    public Optional<LibraryUser> findById(long id) {

        Query managerQuery = entityManager.createQuery("SELECT u FROM LibraryUser u where u.id = :id");
        managerQuery.setParameter("id", id);
        try {
            return Optional.of((LibraryUser) managerQuery.getSingleResult());
        } catch (NoResultException | NonUniqueResultException exc) {
            return Optional.empty();
        }
    }

    public void deleteById(long id) {

        boolean inTransaction = entityManager.isJoinedToTransaction();
        EntityTransaction transaction = entityManager.getTransaction();
        if (!inTransaction) {
            transaction.begin();
        }

        entityManager.remove(entityManager.find(LibraryUser.class, id));

        if (!inTransaction) {
            transaction.commit();
        }

    }


}
