package org.batsunov.Hibernate;


import org.batsunov.Library.UserAction;
import org.batsunov.dao.AuthorDAO;
import org.batsunov.dao.BookDAO;
import org.batsunov.dao.HistoryEntryDAO;
import org.batsunov.dao.LibraryUserDAO;
import org.batsunov.domain.*;

import java.util.concurrent.atomic.AtomicInteger;


public class Main {
    public static void main(String[] args) {

    }

    static void save() {

        Book book = new Book();
        book.setCountOfSheets(100);
        book.setNameOfBook("Name Of Book");
        book.setYearOfWrite(100);

        Author author = new Author();
        author.setPseudonym("Pseudonym");
        author.setName("Dima");
        author.setPatronymic("Patronymic");
        author.setSurname("Surname");

        LibraryUser libraryUser = new LibraryUser();
        libraryUser.setDateOfBirth("26-10-2001");
        libraryUser.setName("d");
        libraryUser.setPatronymic("Serhiiq");
        libraryUser.setSurname("Baaaats");

        HistoryEntry historyEntry = new HistoryEntry();
        historyEntry.setBook(book);
        historyEntry.setUser(libraryUser);
        historyEntry.setUserAction(String.valueOf(UserAction.TAKE));

        AuthorDAO authorDAO = new AuthorDAO();
        BookDAO bookDAO = new BookDAO();
        HistoryEntryDAO historyEntryDAO = new HistoryEntryDAO();
        LibraryUserDAO libraryUserDAO = new LibraryUserDAO();

        authorDAO.save(author);
        book.setAuthor(author);
        libraryUserDAO.save(libraryUser);
        book.setLibraryUser(libraryUser);
        bookDAO.save(book);
        historyEntryDAO.save(historyEntry);


    }
}
