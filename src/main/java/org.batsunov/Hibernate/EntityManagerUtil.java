package org.batsunov.Hibernate;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class EntityManagerUtil {
    private static EntityManager entityManager;
    private static EntityManagerFactory emf;

    private EntityManagerUtil() {
    }

    public static EntityManager getEntityManager() {
        if (entityManager == null) {
            emf = Persistence.createEntityManagerFactory("BookShop");
            entityManager = emf.createEntityManager();
        }
        return entityManager;
    }

    public static void close() {
        entityManager.close();
        emf.close();
    }
}
