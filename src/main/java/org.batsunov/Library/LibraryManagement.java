package org.batsunov.Library;


import org.batsunov.domain.Author;
import org.batsunov.domain.Book;
import org.batsunov.domain.HistoryEntry;
import org.batsunov.domain.LibraryUser;

import java.util.List;
import java.util.Optional;

public interface LibraryManagement {

    void addAuthor(Author authors);

    void deleteAuthor(long id);

    void addBook(Book books);

    void deleteBook(long id);

    void addUser(LibraryUser users);

    void deleteUser(long id);

    void giveBook(LibraryUser user, Book book);

    void returnBook(LibraryUser user, Book book);

    List<HistoryEntry> getHistoryEntryList();

    Optional<Author> getAuthorFromPseudonym(String pseudonym);


}
