package org.batsunov.Library;


import org.batsunov.dao.AuthorDAO;
import org.batsunov.dao.BookDAO;
import org.batsunov.dao.HistoryEntryDAO;
import org.batsunov.dao.LibraryUserDAO;
import org.batsunov.domain.Author;
import org.batsunov.domain.Book;
import org.batsunov.domain.HistoryEntry;
import org.batsunov.domain.LibraryUser;


import java.util.List;
import java.util.Optional;

public class Librarian implements LibraryManagement {

    private static Librarian librarian;

    private AuthorDAO authorDAO = new AuthorDAO();
    private LibraryUserDAO libraryUserDAO = new LibraryUserDAO();
    private HistoryEntryDAO historyEntryDAO = new HistoryEntryDAO();
    private BookDAO bookDAO = new BookDAO();


    private Librarian() {
    }

    public static Librarian getInstance() {

        if (librarian == null) {
            librarian = new Librarian();
        }

        return librarian;
    }


    //domain.Author
    @Override
    public void addAuthor(Author author) {

        authorDAO.save(author);
    }


    @Override
    public void deleteAuthor(long id) {

        authorDAO.deleteById(id);
    }


    public List<Author> getCatalogOfAuthor() {

        return authorDAO.getAll();

    }


    @Override
    public void addBook(Book book) {
        bookDAO.save(book);

    }

    @Override
    public void deleteBook(long id) {

        bookDAO.deleteById(id);

    }


    public List<Book> getCatalogOfBooks() {

        return bookDAO.getAll();

    }

    public List<Book> getCatalogAvailableBook() {
        return bookDAO.getAvailable();
    }

    //User
    @Override
    public void addUser(LibraryUser user) {

        libraryUserDAO.save(user);
    }


    @Override
    public void deleteUser(long id) {

        libraryUserDAO.deleteById(id);

    }

    @Override
    public void giveBook(LibraryUser user, Book book) {
        if (bookDAO.findById(book.getId()).isPresent() &&
                bookDAO.findById(book.getId()).get().getLibraryUser() == null &&
                libraryUserDAO.findById(user.getId()).isPresent()
        ) {
            bookDAO.takeBook(book, user);
        }
    }

    @Override
    public void returnBook(LibraryUser user, Book book) {
        if (bookDAO.findById(book.getId()).isPresent() &&
                libraryUserDAO.findById(user.getId()).isPresent() &&
                bookDAO.findById(book.getId()).get().getLibraryUser().equals(libraryUserDAO.findById(user.getId()).get())
        ) {
            bookDAO.returnBook(book);
        }

    }

    public List<LibraryUser> getCatalogOfUsers() {

        return libraryUserDAO.getAll();

    }

    @Override
    public List<HistoryEntry> getHistoryEntryList() {

        return historyEntryDAO.getAll();
    }

    @Override
    public Optional<Author> getAuthorFromPseudonym(String pseudonym) {
        return authorDAO.findByPseudonym(pseudonym);
    }

}