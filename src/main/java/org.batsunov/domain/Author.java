package org.batsunov.domain;


import javax.persistence.*;
import java.util.Objects;

//Имя, Фамилия, Отчество, Творческий псевдоним
@Entity
@Table(name = "authors")
public class Author extends Person {

    @Id
    @GeneratedValue
    private long id;

    private String pseudonym;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setPseudonym(String pseudonym) {
        this.pseudonym = pseudonym;
    }

    public String getPseudonym() {
        return pseudonym;
    }


    public String getName() {
        return name;
    }


    public String getSurname() {
        return surname;
    }


    public String getPatronymic() {
        return patronymic;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Author author = (Author) o;
        return Objects.equals(name, author.name) &&
                Objects.equals(surname, author.surname) &&
                Objects.equals(patronymic, author.patronymic) &&
                Objects.equals(pseudonym, author.pseudonym);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, surname, patronymic, pseudonym);
    }

    @Override
    public String toString() {
        return "Author{" +
                "id=" + id + ' ' +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", patronymic='" + patronymic + '\'' +
                ", pseudonym='" + pseudonym + '\'' +
                '}';
    }

}