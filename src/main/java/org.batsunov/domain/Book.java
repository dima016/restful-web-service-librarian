package org.batsunov.domain;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "books")
public class Book {

    @Id
    @GeneratedValue
    private long id;

    @Column(nullable = false)
    private String nameOfBook;

    @ManyToOne
    private Author author;

    @Column(name = "YearOfWrite")
    private int yearOfWrite;


    @Column(name = "CountOfSheets", nullable = false)
    private int countOfSheets;

    @JoinColumn
    @OneToOne
    private LibraryUser libraryUser;

    public Long getId() {
        return id;
    }

    public LibraryUser getLibraryUser() {
        return libraryUser;
    }

    public void setLibraryUser(LibraryUser libraryUser) {
        this.libraryUser = libraryUser;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public String getNameOfBook() {
        return nameOfBook;
    }

    public void setNameOfBook(String nameOfBook) {
        this.nameOfBook = nameOfBook;
    }

    public int getYearOfWrite() {
        return yearOfWrite;
    }

    public void setYearOfWrite(int yearOfWrite) {
        this.yearOfWrite = yearOfWrite;
    }

    public int getCountOfSheets() {
        return countOfSheets;
    }

    public void setCountOfSheets(int countOfSheets) {
        this.countOfSheets = countOfSheets;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Book book = (Book) o;
        return yearOfWrite == book.yearOfWrite &&
                countOfSheets == book.countOfSheets &&
                Objects.equals(nameOfBook, book.nameOfBook);
    }


    @Override
    public int hashCode() {
        return Objects.hash(nameOfBook, yearOfWrite, countOfSheets);
    }

    @Override
    public String toString() {
        return "Book{" +
                "id=" + id +
                ", nameOfBook='" + nameOfBook + '\'' +
                ", yearOfWrite=" + yearOfWrite +
                ", countOfSheets=" + countOfSheets +
                '}';
    }
}
