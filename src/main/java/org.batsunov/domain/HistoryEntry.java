package org.batsunov.domain;


import javax.persistence.*;

@Entity
@Table(name = "history_transaction")
public class HistoryEntry {

    @Id
    @GeneratedValue
    private long id;

    @ManyToOne
    private LibraryUser user;

    @ManyToOne
    private Book book;

    @Column
    private String userAction;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public LibraryUser getUser() {
        return user;
    }

    public void setUser(LibraryUser user) {
        this.user = user;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public String getUserAction() {
        return String.valueOf(userAction);
    }

    public void setUserAction(String userAction) {
        this.userAction = userAction;
    }

    @Override
    public String toString() {
        return "domain.HistoryEntry{" +
                "user=" + user +
                ":\n" + book +
                ", status=" + userAction +
                '}';
    }
}

