package org.batsunov.domain;

import javax.persistence.*;
import java.util.Objects;

//Имя, Фамилия, Отчество, Дата рождения, Список книг которые взяты и не возвращены обратно
@Entity
@Table(name = "users")
public class LibraryUser extends Person {

    @Id
    @GeneratedValue
    private long id;

    @Column(nullable = false)
    private String dateOfBirth;

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }



    public String getName() {
        return name;
    }


    public String getSurname() {
        return surname;
    }


    public String getPatronymic() {
        return patronymic;
    }


    public String getDateOfBirth() {
        return dateOfBirth;
    }



    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LibraryUser that = (LibraryUser) o;
        return Objects.equals(name, that.name) &&
                Objects.equals(surname, that.surname) &&
                Objects.equals(patronymic, that.patronymic) &&
                Objects.equals(dateOfBirth, that.dateOfBirth);
    }


    @Override
    public int hashCode() {
        return Objects.hash(name, surname, patronymic, dateOfBirth);
    }


    @Override
    public String toString() {
        return "domain.LibraryUser{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", patronymic='" + patronymic + '\'' +
                ", dateOfBirth='" + dateOfBirth + '\''
                + '}';
    }

}

