package org.batsunov.domain;

import javax.persistence.MappedSuperclass;

@MappedSuperclass
    public abstract class Person {
        String name;
        String surname;
        String patronymic;

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public abstract String getName();

        public abstract String getSurname();

        public abstract String getPatronymic();
}
