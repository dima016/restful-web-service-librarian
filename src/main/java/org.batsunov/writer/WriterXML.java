package org.batsunov.writer;

import org.batsunov.Library.Librarian;
import org.batsunov.domain.HistoryEntry;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;


import java.io.File;
import java.io.IOException;

public class WriterXML {

    public File getHistoryXMLFormat() {
        File tmpFile = null;
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document doc = builder.newDocument();

            Element root = doc.createElementNS("namespace.xsd", "HistoryEntries");
            doc.appendChild(root);

            for (HistoryEntry historyEntry : Librarian.getInstance().getHistoryEntryList()) {
                root.appendChild(createHistory(doc,
                        historyEntry.getId(),
                        historyEntry.getUserAction(),
                        historyEntry.getBook().getId(),
                        historyEntry.getUser().getId()));
            }

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transf = transformerFactory.newTransformer();

            transf.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
            transf.setOutputProperty(OutputKeys.INDENT, "yes");
            transf.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");

            DOMSource source = new DOMSource(doc);

            tmpFile = File.createTempFile("history", ".xml");


            StreamResult file = new StreamResult(tmpFile);
            transf.transform(source, file);

        } catch (ParserConfigurationException | TransformerException | IOException e) {
            e.getStackTrace();
        }
        return tmpFile;
    }

    private static Node createHistory(Document doc, long id, String Action, long bookId, long userId) {
        Element employee = doc.createElement("HistoryEntry");


        employee.setAttribute("action", Action);
        employee.setAttribute("id", String.valueOf(id));
        employee.appendChild(createHistoryEntryElement(doc, "bookID", String.valueOf(bookId)));
        employee.appendChild(createHistoryEntryElement(doc, "userID", String.valueOf(userId)));

        return employee;
    }

    private static Node createHistoryEntryElement(Document doc, String name, String value) {
        Element node = doc.createElement(name);
        node.appendChild(doc.createTextNode(value));

        return node;
    }
}
