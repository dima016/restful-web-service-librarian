package org.batsunov.servlet;

import org.batsunov.Library.Librarian;


import org.batsunov.writer.WriterXML;


import javax.servlet.*;
import javax.servlet.annotation.WebServlet;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;

import java.util.ResourceBundle;

import static org.batsunov.constants.Constants.LOCALE;


@WebServlet(name = "All history", urlPatterns = {"/history"})
public class HistoryListServlet extends HttpServlet {

    private Librarian librarian = Librarian.getInstance();
    private ResourceBundle bundle = ResourceBundle.getBundle("showDomains", LOCALE);
    private ResourceBundle bundleDomain = ResourceBundle.getBundle("domain", LOCALE);
    private WriterXML writerXML = new WriterXML();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        req.setCharacterEncoding("UTF-8");
        req.setAttribute("bundle", bundle);
        req.setAttribute("bundleDomain", bundleDomain);
        req.setAttribute("history", librarian.getHistoryEntryList());
        req.getRequestDispatcher("/WEB-INF/jsp/listOfDomain/historyList.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        File historyFileXML = writerXML.getHistoryXMLFormat();

        String mimeType = historyFileXML.toURL().openConnection().getContentType();

        if (mimeType == null) {
            mimeType = "application/octet-stream";
        }

        resp.setContentType(mimeType);
        resp.setContentLengthLong(historyFileXML.length());

        String headerKey = "Content-Disposition";
        String headerValue = String.format("attachment; filename=\"%s\"", historyFileXML.getName());
        resp.setHeader(headerKey, headerValue)   ;

        OutputStream outStream = resp.getOutputStream();

        byte[] buffer = new byte[4096];
        int bytesRead = -1;

        FileInputStream inStream = new FileInputStream(historyFileXML);
        while ((bytesRead = inStream.read(buffer)) != -1) {
            outStream.write(buffer, 0, bytesRead);
        }

        inStream.close();
        outStream.close();
    }
}