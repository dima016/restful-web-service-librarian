package org.batsunov.servlet;


import org.batsunov.Library.Librarian;
import org.batsunov.constants.Constants;
import org.batsunov.dao.BookDAO;
import org.batsunov.dao.LibraryUserDAO;
import org.batsunov.domain.Book;
import org.batsunov.domain.LibraryUser;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ResourceBundle;

@WebServlet(name = "User Account", urlPatterns = {"/user/cabinet"})
public class UserCabinetServlet extends HttpServlet {

    private Librarian librarian = Librarian.getInstance();
    private LibraryUserDAO libraryUserDAO = new LibraryUserDAO();
    private ResourceBundle bundle1 = ResourceBundle.getBundle("showDomains", Constants.LOCALE);
    private ResourceBundle bundle2 = ResourceBundle.getBundle("takeBook", Constants.LOCALE);
    private ResourceBundle bundleDomain = ResourceBundle.getBundle("domain", Constants.LOCALE);
    private BookDAO bookDAO = new BookDAO();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        req.setCharacterEncoding("UTF-8");

        Long id = Long.valueOf(req.getParameter("id"));

        LibraryUser user = libraryUserDAO.findById((id)).get();

        req.setAttribute("user", user);
        req.setAttribute("bundle", bundle1);
        req.setAttribute("userBook", libraryUserDAO.getDebtById(id));
        req.setAttribute("bundleDomain", bundleDomain);
        req.getRequestDispatcher("/WEB-INF/jsp/userCabinet.jsp").forward(req, resp);
    }


    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");


        Long id = Long.valueOf(req.getParameter("id"));

        req.setAttribute("id", id);
        req.setAttribute("bundle", bundle2);
        req.setAttribute("books", bookDAO.getAvailable());
        req.setAttribute("bundleDomain", bundleDomain);
        req.getRequestDispatcher("/WEB-INF/jsp/historyForTakeBook.jsp").forward(req, resp);
    }


}
