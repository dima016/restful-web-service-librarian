package org.batsunov.servlet;

import org.batsunov.Library.Librarian;
import org.batsunov.constants.Constants;

import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import java.io.IOException;
import java.util.ResourceBundle;

@WebServlet(name = "All users", urlPatterns = {"/users"})
public class UsersListServlet implements Servlet {

    private ResourceBundle bundle = ResourceBundle.getBundle("showDomains", Constants.LOCALE);
    private ResourceBundle bundleDomain = ResourceBundle.getBundle("domain", Constants.LOCALE);


    private ServletConfig config = null;



    @Override
    public void init(ServletConfig servletConfig) throws ServletException {
        config = config;
    }

    @Override
    public ServletConfig getServletConfig() {
        return this.config;
    }

    @Override
    public void service(ServletRequest req, ServletResponse resp) throws ServletException, IOException {

        req.setAttribute("users", Librarian.getInstance().getCatalogOfUsers());
        req.setAttribute("bundle", bundle);
        req.setAttribute("bundleDomain",bundleDomain);
        req.getRequestDispatcher("/WEB-INF/jsp/listOfDomain/userList.jsp").forward(req, resp);

    }

    @Override
    public String getServletInfo() {
        return "Servlet for showing users";
    }

    @Override
    public void destroy() {

    }
}
