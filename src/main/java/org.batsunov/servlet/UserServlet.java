package org.batsunov.servlet;


import org.batsunov.Library.Librarian;
import org.batsunov.domain.LibraryUser;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.ResourceBundle;

import static org.batsunov.constants.Constants.LOCALE;


@WebServlet(name = "User Registration Servlet", urlPatterns = {"/register/user"})
public class UserServlet extends HttpServlet {

    private Librarian librarian = Librarian.getInstance();
    private ResourceBundle bundle = ResourceBundle.getBundle("formRegForDomain", LOCALE);
    private ResourceBundle bundleDomain = ResourceBundle.getBundle("domain",LOCALE);
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        req.setCharacterEncoding("UTF-8");
        req.setAttribute("bundle", bundle);
        req.setAttribute("bundleDomain",bundleDomain);
        req.getRequestDispatcher("/WEB-INF/jsp/domain/user.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        String name = req.getParameter("firstName");
        String surname = req.getParameter("secondName");
        String patronymic = req.getParameter("patronymic");
        String date = req.getParameter("dateOfBirth");

        if (date.isBlank() || name.isBlank() || surname.isBlank()) {
            req.setAttribute("errors", true);
            req.getRequestDispatcher("/WEB-INF/jsp/domain/user.jsp").forward(req, resp);
        } else {

            LibraryUser libraryUser = new LibraryUser();
            libraryUser.setDateOfBirth(date);
            libraryUser.setName(name);
            libraryUser.setPatronymic(patronymic);
            libraryUser.setSurname(surname);

            librarian.addUser(libraryUser);

            MessageFormat format = new MessageFormat(bundle.getString("user.successReg"), LOCALE);

            String message = format.format(new Object[]{name,surname});

            req.setAttribute("message", message);
            req.setAttribute("bundle", bundle);
            req.getRequestDispatcher("/WEB-INF/jsp/finishRegistration/regFinished_User.jsp").forward(req, resp);
        }
    }
}
