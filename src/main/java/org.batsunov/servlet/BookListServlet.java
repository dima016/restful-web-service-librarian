package org.batsunov.servlet;

import org.batsunov.Library.Librarian;
import org.batsunov.constants.Constants;
import org.batsunov.domain.Book;

import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ResourceBundle;

@WebServlet(name = "All books", urlPatterns = {"/books"})
public class BookListServlet implements Servlet {

    private ServletConfig config = null;
    private ResourceBundle bundle = ResourceBundle.getBundle("showDomains", Constants.LOCALE);
    private ResourceBundle bundleDomain = ResourceBundle.getBundle("domain", Constants.LOCALE);

    private Librarian librarian = Librarian.getInstance();


    @Override
    public void init(ServletConfig servletConfig) throws ServletException {
        config = config;
    }

    @Override
    public ServletConfig getServletConfig() {
        return this.config;
    }

    @Override
    public void service(ServletRequest servletRequest, ServletResponse servletResponse) throws ServletException, IOException {

        servletRequest.setCharacterEncoding("UTF-8");
        servletRequest.setAttribute("books", librarian.getCatalogOfBooks());
        servletRequest.setAttribute("bundle", bundle);
        servletRequest.setAttribute("bundleDomain",bundleDomain);

        servletRequest.getRequestDispatcher("/WEB-INF/jsp/listOfDomain/bookList.jsp").forward(servletRequest, servletResponse);
    }

    @Override
    public String getServletInfo() {
        return "Servlet for showing book";
    }

    @Override
    public void destroy() {

    }

}


