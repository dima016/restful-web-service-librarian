package org.batsunov.servlet;

import org.batsunov.Library.Librarian;
import org.batsunov.domain.Author;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.ResourceBundle;

import static org.batsunov.constants.Constants.LOCALE;

@WebServlet(name = "Author Registration Form", urlPatterns = {"/register/author"})
public class AuthorServlet extends HttpServlet {


    private Librarian librarian = Librarian.getInstance();
    private ResourceBundle bundle = ResourceBundle.getBundle("formRegForDomain", LOCALE);
    private ResourceBundle bundleDomain = ResourceBundle.getBundle("domain", LOCALE);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        req.setAttribute("bundle", bundle);
        req.setAttribute("bundleDomain", bundleDomain);

        req.getRequestDispatcher("/WEB-INF/jsp/domain/author.jsp").forward(req, resp);

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {


        req.setCharacterEncoding("UTF-8");

        String name = req.getParameter("name");
        String surname = req.getParameter("surname");
        String patronymic = req.getParameter("patronymic");
        String authorPseudonym = req.getParameter("authorPseudonym");


        if (name.isBlank() || surname.isBlank()) {
            req.setAttribute("errors", true);
            req.getRequestDispatcher("/WEB-INF/jsp/domain/author.jsp").forward(req, resp);
        } else {

            Author author = new Author();
            if (authorPseudonym == "") {
                authorPseudonym="-";
            }

            author.setPseudonym(authorPseudonym);
            author.setName(name);
            author.setSurname(surname);
            author.setPatronymic(patronymic);

            librarian.addAuthor(author);


            MessageFormat format = new MessageFormat(bundle.getString("author.successReg"), LOCALE);

            String message = format.format(new Object[]{name, surname});

            req.setAttribute("message", message);
            req.setAttribute("bundle", bundle);

            req.getRequestDispatcher("/WEB-INF/jsp/finishRegistration/regFinished_Author.jsp").forward(req, resp);
        }
    }


}

