package org.batsunov.servlet;

import org.batsunov.Library.Librarian;
import org.batsunov.constants.Constants;
import org.batsunov.dao.BookDAO;
import org.batsunov.dao.LibraryUserDAO;
import org.batsunov.domain.Book;
import org.batsunov.domain.LibraryUser;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import java.text.MessageFormat;
import java.util.ResourceBundle;

@WebServlet(name = "Return Book", urlPatterns = {"/return"})
public class ReturnBookServlet extends HttpServlet {
    private Librarian librarian = Librarian.getInstance();
    private BookDAO bookDAO = new BookDAO();
    private LibraryUserDAO libraryUserDAO = new LibraryUserDAO();
    private ResourceBundle bundle = ResourceBundle.getBundle("returnBook", Constants.LOCALE);


    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        req.setCharacterEncoding("UTF-8");

        long idUser = Long.parseLong(req.getParameter("id"));
        long idBook = Long.parseLong(req.getParameter("book"));

        Book book = bookDAO.findById(idBook).get();
        LibraryUser user = libraryUserDAO.findById(idUser).get();

        librarian.returnBook(user, book);

        MessageFormat format = new MessageFormat(bundle.getString("message"), Constants.LOCALE);

        String message = format.format(new Object[]{user.getName(), user.getSurname(), book.getNameOfBook()});

        req.setAttribute("nameUser", user.getName());
        req.setAttribute("surnameUser", user.getSurname());
        req.setAttribute("bookName", book.getNameOfBook());
        req.setAttribute("bundle", bundle);
        req.setAttribute("message", message);
        req.getRequestDispatcher("/WEB-INF/jsp/doneReturnBook.jsp").forward(req, resp);

    }

}
