package org.batsunov.servlet;

import org.batsunov.Library.Librarian;
import org.batsunov.domain.Book;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.ResourceBundle;

import static org.batsunov.constants.Constants.LOCALE;

@WebServlet(name = "Book Registration Form", urlPatterns = {"/register/book"})
public class BookServlet extends HttpServlet {

    private Librarian librarian = Librarian.getInstance();

    private ResourceBundle bundle = ResourceBundle.getBundle("formRegForDomain", LOCALE);
    private ResourceBundle bundleDomain = ResourceBundle.getBundle("domain", LOCALE);


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        req.setCharacterEncoding("UTF-8");

        req.setAttribute("bundle", bundle);
        req.setAttribute("bundleDomain", bundleDomain);
        req.getRequestDispatcher("/WEB-INF/jsp/domain/book.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        req.setCharacterEncoding("UTF-8");
        String nameOfBook = req.getParameter("nameOfBook");
        String yearOfWrite = req.getParameter("yearOfWrite");
        String countOfSheets = req.getParameter("countOfSheets");
        String authorPseudonym = req.getParameter("authorPseudonym");


        if (nameOfBook.isBlank() || countOfSheets.isBlank()) {
            req.setAttribute("errors", true);
            req.getRequestDispatcher("/WEB-INF/jsp/domain/book.jsp").forward(req, resp);
        } else {

            Book book = new Book();
            book.setNameOfBook(nameOfBook);
            book.setCountOfSheets(Integer.parseInt(countOfSheets));
            book.setYearOfWrite(Integer.parseInt(yearOfWrite));

            if (librarian.getAuthorFromPseudonym(authorPseudonym).isPresent()) {
                book.setAuthor(librarian.getAuthorFromPseudonym(authorPseudonym).get());
            }

            librarian.addBook(book);

            MessageFormat format = new MessageFormat(bundle.getString("book.successReg"), LOCALE);

            String message = format.format(new Object[]{nameOfBook});

            req.setAttribute("message", message);
            req.setAttribute("bundle", bundle);

            req.getRequestDispatcher("/WEB-INF/jsp/finishRegistration/regFinished_Book.jsp").forward(req, resp);
        }
    }
}
