package org.batsunov.servlet;

import org.batsunov.Library.Librarian;
import org.batsunov.constants.Constants;

import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import java.io.IOException;
import java.util.ResourceBundle;

@WebServlet(name = "All authors", urlPatterns = {"/authors"})
public class AuthorListServlet implements Servlet {


    private ServletConfig config = null;
    private ResourceBundle bundle = ResourceBundle.getBundle("showDomains", Constants.LOCALE);
    private ResourceBundle bundleDomain = ResourceBundle.getBundle("domain", Constants.LOCALE);

    private Librarian librarian = Librarian.getInstance();


    @Override
    public void init(ServletConfig servletConfig) throws ServletException {
        config = config;
    }

    @Override
    public ServletConfig getServletConfig() {
        return this.config;
    }

    @Override
    public void service(ServletRequest servletRequest, ServletResponse servletResponse) throws ServletException, IOException {

        servletRequest.setCharacterEncoding("UTF-8");
        servletRequest.setAttribute("authors", librarian.getCatalogOfAuthor());
        servletRequest.setAttribute("bundle", bundle);
        servletRequest.setAttribute("bundleDomain",bundleDomain);
        servletRequest.getRequestDispatcher("/WEB-INF/jsp/listOfDomain/authorList.jsp").forward(servletRequest, servletResponse);
    }

    @Override
    public String getServletInfo() {
        return "Servlet for showing author";
    }

    @Override
    public void destroy() {

    }

}
