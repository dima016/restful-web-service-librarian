package org.batsunov.controller;


import org.batsunov.Library.Librarian;
import org.batsunov.dao.AuthorDAO;
import org.batsunov.domain.Author;
import org.batsunov.domain.Book;


import javax.inject.Singleton;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.net.URI;
import java.util.List;
import java.util.Optional;

@Path("/author")
@Singleton
public class AuthorController {

    @Context
    private UriInfo uriInfo;

    private final Librarian librarian = Librarian.getInstance();

    private final AuthorDAO authorDAO = new AuthorDAO();

    @POST
    public Response create(Author domain) {
        librarian.addAuthor(domain);
        return Response.created(URI.create(uriInfo.getAbsolutePath().toString() + '/' + domain.getId())).build();
    }

    @GET
    public List<Author> getAll() {
        return librarian.getCatalogOfAuthor();
    }

    @GET
    @Path("/query")
    public Optional<Author> findByPseudonym(@QueryParam("pseudonym") String pseudonym) {
        return librarian.getAuthorFromPseudonym(pseudonym);
    }

    @GET
    @Path("/{id}")
    public Response findById(@PathParam("id") long id) {
        Optional<Author> cardOpt = authorDAO.findById(id);
        if (cardOpt.isPresent()) {
            return Response.ok(cardOpt.get()).build();
        } else {
            throw new NotFoundException();
        }
    }

    @DELETE
    @Path("/{id}")
    public Response remove(@PathParam("id") long id) {
        for (Book book : librarian.getCatalogOfBooks()) {
            if (authorDAO.findById(id).isPresent() && book.getAuthor().equals(authorDAO.findById(id).get())) {
                return Response.noContent().build();
            }
        }

        authorDAO.deleteById(id);
        return Response.ok().build();
    }
}
