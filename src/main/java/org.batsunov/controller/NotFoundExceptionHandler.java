package org.batsunov.controller;


import javassist.NotFoundException;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import java.util.List;

@Provider
public class NotFoundExceptionHandler implements ExceptionMapper<NotFoundException> {

    @Context
    private HttpHeaders headers;

    @Override
    public Response toResponse(NotFoundException ex) {
        return Response.status(404).entity("").type( getAcceptType()).build();
    }

    private String getAcceptType(){
        List<MediaType> accepts = headers.getAcceptableMediaTypes();
        if (accepts != null && accepts.size() > 0) {
            return accepts.get(0).toString();
        } else {
            return MediaType.APPLICATION_JSON;
        }
    }
}