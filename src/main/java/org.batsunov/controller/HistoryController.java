package org.batsunov.controller;


import javax.inject.Singleton;
import javax.ws.rs.*;
import java.util.List;


import org.batsunov.Library.Librarian;
import org.batsunov.dao.HistoryEntryDAO;
import org.batsunov.domain.Book;
import org.batsunov.domain.HistoryEntry;
import org.batsunov.domain.LibraryUser;




@Path("/history")
@Singleton
public class HistoryController {

    private final HistoryEntryDAO historyEntryDAO = new HistoryEntryDAO();

    private final Librarian librarian = Librarian.getInstance();

    @GET
    @Path("/getAll")
    public List<HistoryEntry> getAll() {

        return librarian.getHistoryEntryList();
    }

    @GET
    @Path("/getByBook")
    public List<HistoryEntry> getByBook(Book book) {

        return historyEntryDAO.getHistoryByBook(book);
    }

    @GET
    @Path("/getByUser")
    public List<HistoryEntry> getByUser(LibraryUser libraryUser){

        return historyEntryDAO.getHistoryByUser(libraryUser);
    }

}
