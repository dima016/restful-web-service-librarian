package org.batsunov.controller;


import javax.inject.Singleton;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.net.URI;
import java.util.List;
import java.util.Optional;

import org.batsunov.Library.Librarian;
import org.batsunov.dao.BookDAO;
import org.batsunov.dao.LibraryUserDAO;
import org.batsunov.domain.Book;
import org.batsunov.domain.LibraryUser;


@Path("/user")
@Singleton
public class UserController {

    @Context
    private UriInfo uriInfo;

    private BookDAO bookDAO = new BookDAO();

    private final LibraryUserDAO userDAO = new LibraryUserDAO();

    private final Librarian librarian = Librarian.getInstance();


    @POST
    public Response create(LibraryUser domain) {
        librarian.addUser(domain);
        return Response.created(URI.create(uriInfo.getAbsolutePath().toString() + '/' + domain.getId())).build();
    }

    @GET
    public List<LibraryUser> getAll() {
        return librarian.getCatalogOfUsers();
    }

    @GET
    @Path("/query")
    public Optional<LibraryUser> findByName(@QueryParam("name") String name) {
        return userDAO.findByName(name);
    }

    @GET
    @Path("/{id}")
    public Response findById(@PathParam("id") long id) {
        Optional<LibraryUser> cardOpt = userDAO.findById(id);
        if (cardOpt.isPresent()) {
            return Response.ok(cardOpt.get()).build();
        } else {
            throw new NotFoundException();
        }
    }

    @DELETE
    @Path("/{id}")
    public Response remove(@PathParam("id") long id) {
        for (Book book : bookDAO.getAll()) {
            if (userDAO.findById(id).isPresent() && book.getLibraryUser().equals(userDAO.findById(id).get())) {
                return Response.noContent().build();
            }
        }
        librarian.deleteBook(id);
        return Response.ok().build();

    }


}
