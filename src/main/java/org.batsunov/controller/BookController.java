package org.batsunov.controller;


import javax.inject.Singleton;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.net.URI;
import java.util.List;
import java.util.Optional;

import org.batsunov.Library.Librarian;
import org.batsunov.dao.BookDAO;
import org.batsunov.dao.LibraryUserDAO;
import org.batsunov.domain.Book;
import org.batsunov.domain.LibraryUser;


@Path("/book")
@Singleton
public class BookController {


    @Context
    private UriInfo uriInfo;

    private final BookDAO bookDAO = new BookDAO();

    private final Librarian librarian = Librarian.getInstance();
    private LibraryUserDAO libraryUserDAO = new LibraryUserDAO();

    @POST
    public Response create(Book domain) {
        librarian.addBook(domain);
        return Response.created(URI.create(uriInfo.getAbsolutePath().toString() + '/' + domain.getId())).build();
    }

    @GET
    public List<Book> getAll() {
        return librarian.getCatalogOfBooks();
    }

    @GET
    @Path("/{id}")
    public Response findById(@PathParam("id") long id) {
        Optional<Book> cardOpt = bookDAO.findById(id);

        if (cardOpt.isPresent()) {
            return Response.ok(cardOpt.get()).build();
        } else {
            throw new NotFoundException();
        }
    }

    @DELETE
    @Path("/{id}")
    public Response remove(@QueryParam("id") long id) {

        if (bookDAO.findById(id).isPresent() && bookDAO.findById(id).get().getLibraryUser() == null) {
            librarian.deleteBook(id);
            return Response.ok().build();
        } else {
            return Response.noContent().build();
        }

    }

    @GET
    @Path("/available")
    public List<Book> getAvailable() {
        return bookDAO.getAvailable();
    }

    //Возможность обновлять книгу с помощью PATCH запроса (запрос принимает объект {"user": $user_id}). \
    // Если передается не NULL овый юзер, то добавлять в историю запись о том что пользователь взял книгу,
    // если записывается NULL то записывать в историю что пользователь вернул книгу (PATCH)

    @PATCH
    @Path("/{idBook}")
    public Response update(@PathParam("idBook") long id, LibraryUser libraryUser) { // не понял какую  именно книгу апдейтим,поэтому поставил по ид,даже если зайти в долг юзера,там же не одна книга может быть

        if (bookDAO.findById(id).isPresent() && bookDAO.findById(id).get().getLibraryUser() == null && libraryUserDAO.findById(libraryUser.getId()).isPresent()) {
            bookDAO.takeBook(bookDAO.findById(id).get(), libraryUser);
        } else if (bookDAO.findById(id).isPresent() && libraryUser == null) {
            bookDAO.returnBook(bookDAO.findById(id).get());
        } else throw new NotFoundException();

        return Response.ok().build();
    }

}
