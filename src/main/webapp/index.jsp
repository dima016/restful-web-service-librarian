<%@ page import="org.batsunov.statistics.Statistics" %>
<%@ page import="java.util.ResourceBundle" %>
<%@ page import="org.batsunov.constants.Constants" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%session.setAttribute("bundle", ResourceBundle.getBundle("homePage",Constants.LOCALE));%>
<html>
<style>
    .center-img {
        display: block;
        margin: 0 auto;
    }

    .button {
        text-align: center;
    }
</style>
<body>

<h1 align="center">${bundle.getString("homePage.greeting")}</h1>

<div class="button">
    <a href="${pageContext.request.contextPath}/register/user">${bundle.getString("homePage.reg.user")}</a>
    <a href="${pageContext.request.contextPath}/register/book">${bundle.getString("homePage.reg.book")}</a>
    <a href="${pageContext.request.contextPath}/register/author">${bundle.getString("homePage.reg.author")}</a>
    <br>
    <a href="${pageContext.request.contextPath}/users">${bundle.getString("homePage.show.user")}</a>
    <a href="${pageContext.request.contextPath}/books">${bundle.getString("homePage.show.book")}</a>
    <a href="${pageContext.request.contextPath}/authors">${bundle.getString("homePage.show.author")}</a>
    <br>
    <a href="${pageContext.request.contextPath}/history">${bundle.getString("homePage.show.history")}</a>

</div>

<img src="static/img/alfons-morales-YLSwjSy7stw-unsplash.jpg" alt="library logo" width="500"
     height="300" class="center-img">

<hr>

<h3 align="center"> ${bundle.getString("homePage.statistic")}</h3>
<h4 align="center">${bundle.getString("homePage.registered")}</h4>

<p align="center">${bundle.getString("homePage.statistic.user")}<%out.println(Statistics.numberOfAllUsers());%></p>
<p align="center">${bundle.getString("homePage.statistic.bookRegistered")}<%out.println(Statistics.numberOfAllBook());%></p>
<p align="center">${bundle.getString("homePage.statistic.bookAvailable")}<%out.println(Statistics.getAvailableBook());%></p>
<p align="center">${bundle.getString("homePage.statistic.bookOnReading")}<%out.println(Statistics.getNumberBooksInUse());%></p>


</body>
</html>
