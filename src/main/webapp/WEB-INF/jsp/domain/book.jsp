<%@ page contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <title>Register Book</title>
</head>
<body>

<h2> ${bundle.getString("book.registration")} </h2>

<% if (request.getAttribute("errors") != null) { %>
<h4>${bundle.getString("errors")}</h4>
<% } %>

<form action="${pageContext.request.contextPath}/register/book" method="post">

    <label for="nameOfBook">${bundleDomain.getString("book.name")}</label>
    <input id="nameOfBook" type="text" name="nameOfBook">
    <br> <br>

    <label for="yearOfWrite">${bundleDomain.getString("book.yearOfWrite")}</label>
    <input id="yearOfWrite" type="number" name="yearOfWrite">
    <br> <br>

    <label for="countOfSheets">${bundleDomain.getString("book.countOfSheets")} </label>
    <input id="countOfSheets" type="number" name="countOfSheets">
    <br> <br>

    <label for="authorPseudonym">${bundleDomain.getString("book.authorPseudonym")} </label>
    <input id="authorPseudonym" type="text" name="authorPseudonym"><br>
    <br>
    <input type="submit" value="${bundle.getString("sent")}">
</form>

<form action="${pageContext.request.contextPath}" method="post">

    <input type="submit" value="${bundle.getString("homePage")}">
</form>
</body>
</html>