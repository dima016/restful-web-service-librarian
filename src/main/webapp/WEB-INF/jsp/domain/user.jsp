<%@ page contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <title>Register User</title>
</head>
<body>

<h2> ${bundle.getString("user.registration")} </h2>

<% if (request.getAttribute("errors") != null) { %>
<h4>${bundle.getString("errors")}</h4>
<% } %>
<form action="${pageContext.request.contextPath}/register/user" method="post">

    <label for="firstName">${bundleDomain.getString("user.name")} </label>
    <input id="firstName" type="text" name="firstName">
    <br> <br>

    <label for="secondName">${bundleDomain.getString("user.surname")} </label>
    <input id="secondName" type="text" name="secondName">
    <br> <br>

    <label for="patronymic">${bundleDomain.getString("user.patronymic")}</label>
    <input id="patronymic" type="text" name="patronymic">
    <br> <br>

    <label for="dateOfBirth">${bundleDomain.getString("user.dateOfBirthday")}</label>
    <input id="dateOfBirth" type="date" name="dateOfBirth"><br>
    <br>
    <input type="submit" value="${bundle.getString("sent")}">
    <br>

</form>

<form action="${pageContext.request.contextPath}" method="post">
    <input type="submit" value="${bundle.getString("homePage")}">
</form>
</body>
</html>