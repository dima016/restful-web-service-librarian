<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>Author Register Form</title>
</head>
<body>
<h2> ${bundle.getString("author.registration")} </h2>

<% if (request.getAttribute("errors") != null) { %>
<h4>${bundle.getString("errors")}</h4>
<% } %>

<form action="${pageContext.request.contextPath}/register/author" method="post" >

    <label for="name">${bundleDomain.getString("author.name")} : </label>
    <input id="name" type="text" name="name">
    <br> <br>

    <label for="surname">${bundleDomain.getString("author.surname")} :</label>
    <input id="surname" type="text" name="surname">
    <br> <br>

    <label for="patronymic">${bundleDomain.getString("author.patronymic")}:</label>
    <input id="patronymic" type="text" name="patronymic">
    <br> <br>

    <label for="authorPseudonym">${bundleDomain.getString("author.pseudonym")}:</label>
    <input id="authorPseudonym" type="text" name="authorPseudonym"><br>
    <br>
    <input type="submit" value="${bundle.getString("sent")}">
</form>

<form action="${pageContext.request.contextPath}" method="post">

    <input type="submit" value="${bundle.getString("homePage")}">
</form>

</body>
</html>
