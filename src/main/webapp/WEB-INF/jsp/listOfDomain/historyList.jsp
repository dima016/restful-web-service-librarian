<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>History List</title>
    <style>
        table {
            width: 85%;
            border-collapse: collapse;
        }

        th {
            background: #afd792;
            color: #333;
        }
        table td:hover {
            background: lightgray;
        }

        body tr:hover {
            background: grey;
            color: #fff;
        }
    </style>
</head>
<body>
<h1>${bundle.getString("history")}</h1>

<table border="1">
    <tr>
        <th>ID</th>
        <th>${bundleDomain.getString("historyEntry.userId")}</th>
        <th>${bundleDomain.getString("historyEntry.bookId")}</th>
        <th>${bundleDomain.getString("historyEntry.action")}</th>
    </tr>
    <c:forEach var="el" items="${history}">
        <tr>
            <td><c:out value="${el.getId()}"></c:out></td>
            <td><c:out value="${el.getUser().getId()}"></c:out></td>
            <td><c:out value="${el.getBook().getId()}"></c:out></td>
            <td><c:out value="${el.getUserAction()}"></c:out></td>
        </tr>
    </c:forEach>
</table>
<form action="${pageContext.request.contextPath}/history" method="post">
    <input type="submit" value="${bundle.getString("download")}">
</form>
<form action="${pageContext.request.contextPath}" method="post">

    <input type="submit" value="${bundle.getString("homePage")}">
</form>
</body>
</html>
