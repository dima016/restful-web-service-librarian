<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>UsersList</title>
    <style>
        table {
            width: 85%;
            border-collapse: collapse;
        }

        th {
            background: #afd792;
            color: #333;
        }
        table td:hover {
            background: lightgray;
        }

        body tr:hover {
            background: grey;
            color: #fff;
        }
    </style>
</head>
<body>
<h1>${bundle.getString("user.tables")}</h1>

<table border="1">
    <tr>
        <th>ID</th>
        <th>${bundleDomain.getString("user.name")}</th>
        <th>${bundleDomain.getString("user.surname")}</th>
        <th>${bundleDomain.getString("user.patronymic")}</th>
        <th>${bundleDomain.getString("user.dateOfBirthday")}</th>
        <th>${bundle.getString("user.cabinet")}</th>
    </tr>
    <c:forEach var="user" items="${users}">
        <tr>
            <td><c:out value="${user.getId()}"></c:out></td>
            <td><c:out value="${user.getName()}"></c:out></td>
            <td><c:out value="${user.getSurname()}"></c:out></td>
            <td><c:out value="${user.getPatronymic()}"></c:out></td>
            <td><c:out value="${user.getDateOfBirth()}"></c:out></td>
            <td>
                <a href="${pageContext.request.contextPath}/user/cabinet?id=${user.getId()}">${bundle.getString("goOver")}</a>
            </td>
        </tr>
    </c:forEach>
</table>
<form action="${pageContext.request.contextPath}" method="post">

    <input type="submit" value="${bundle.getString("homePage")}">
</form>
</body>
</html>
