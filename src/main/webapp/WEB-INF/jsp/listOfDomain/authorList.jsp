<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Author List</title>
    <style>
        table {
            width: 85%;
            border-collapse: collapse;
        }

        th {
            background: #afd792;
            color: #333;
        }
        table td:hover {
            background: lightgray;
        }

        body tr:hover {
            background: grey;
            color: #fff;
        }
    </style>
</head>
<body>

<h1>${bundle.getString("author.tables")}</h1>
<table border="1">
    <tr>
        <th>ID</th>
        <th>${bundleDomain.getString("author.name")}</th>
        <th>${bundleDomain.getString("author.surname")}</th>
        <th>${bundleDomain.getString("author.patronymic")}</th>
        <th>${bundleDomain.getString("author.pseudonym")}</th>
    </tr>
    <c:forEach var="author" items="${authors}">
        <tr>
            <td><c:out value="${author.getId()}"></c:out></td>
            <td><c:out value="${author.getName()}"></c:out></td>
            <td><c:out value="${author.getSurname()}"></c:out></td>
            <td><c:out value="${author.getPatronymic()}"></c:out></td>
            <td><c:out value="${author.getPseudonym()}"></c:out></td>
        </tr>
    </c:forEach>

</table>
<form action="${pageContext.request.contextPath}" method="post">

    <input type="submit" value="${bundle.getString("homePage")}">
</form>

</body>
</html>
