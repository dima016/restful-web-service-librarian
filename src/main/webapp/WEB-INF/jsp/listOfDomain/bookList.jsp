<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Book List</title>
    <style>
        table {
            width: 85%;
            border-collapse: collapse;
        }

        th {
            background: #afd792;
            color: #333;
        }
        table td:hover {
            background: lightgray;
        }

        body tr:hover {
            background: grey;
            color: #fff;
        }
    </style>
</head>
<body>

<h1>${bundle.getString("book.tables")}</h1>
<table border="1">
    <tr>
        <th>ID</th>
        <th>${bundleDomain.getString("book.name")}</th>
        <th>${bundleDomain.getString("book.yearOfWrite")}</th>
        <th>${bundleDomain.getString("book.countOfSheets")}</th>
        <th>${bundleDomain.getString("book.authorId")}</th>
        <th>${bundleDomain.getString("book.userId")}</th>

    </tr>
    <c:forEach var="book" items="${books}">
        <tr>
            <td><c:out value="${book.getId()}"></c:out></td>
            <td><c:out value="${book.getNameOfBook()}"></c:out></td>
            <td><c:out value="${book.getYearOfWrite()}"></c:out></td>
            <td><c:out value="${book.getCountOfSheets()}"></c:out></td>
            <td><c:out value="${book.getAuthor().getId()}"></c:out></td>
            <td><c:out value="${book.getLibraryUser().getId()}"></c:out></td>

        </tr>
    </c:forEach>

</table>
<form action="${pageContext.request.contextPath}" method="post">

    <input type="submit" value="${bundle.getString("homePage")}">
</form>

</body>
</html>
