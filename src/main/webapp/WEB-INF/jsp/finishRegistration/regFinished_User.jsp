<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>Registration finished</title>
</head>
<body>
<h1>${bundle.getString("congratulate")}</h1>
<p>${message}</p>

<form action="${pageContext.request.contextPath}" method="post">

    <input type="submit" value="${bundle.getString("homePage")}">
</form>
</body>
</html>

