<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <style>
        table {
            width: 85%;
            border-collapse: collapse;
        }

        th {
            background: #afd792;
            color: #333;
        }
        table td:hover {
            background: lightgray;
        }

        body tr:hover {
            background: grey;
            color: #fff;
        }
    </style>
</head>
<body>
<h1>${bundle.getString("user.cabinet")}</h1>
<b>ID:<c:out value="${user.getId()}"></c:out></b>
<br>
<b>${bundleDomain.getString("user.name")}:<c:out value="${user.getName()}"></c:out></b>
<br>
<b>${bundleDomain.getString("user.surname")}:<c:out value="${user.getSurname()}"></c:out></b>
<br>
<b>${bundleDomain.getString("user.patronymic")}:<c:out value="${user.getPatronymic()}"></c:out></b>
<br>
<b>${bundleDomain.getString("user.dateOfBirthday")}:<c:out value="${user.getDateOfBirth()}"></c:out></b>
<br>

<table border="1">
    <tr>
        <th>Id</th>
        <th>${bundleDomain.getString("book.name")}</th>
        <th>${bundleDomain.getString("book.yearOfWrite")}</th>
        <th>${bundleDomain.getString("book.countOfSheets")}</th>
        <th>${bundleDomain.getString("book.authorId")}</th>
        <th>${bundle.getString("action")}</th>
    </tr>
    <c:forEach var="book" items="${userBook}">
        <tr>
            <td><c:out value="${book.getId()}"></c:out></td>
            <td><c:out value="${book.getNameOfBook()}"></c:out></td>
            <td><c:out value="${book.getYearOfWrite()}"></c:out></td>
            <td><c:out value="${book.getCountOfSheets()}"></c:out></td>
            <td><c:out value="${book.getAuthor().getId()}"></c:out></td>
            <td><form action="${pageContext.request.contextPath}/return?id=${user.getId()}&book=${book.getId()}" method="post">
                <input type="submit" value="${bundle.getString("return")}">
            </form>
            </td>
        </tr>
    </c:forEach>
</table>
<form action="${pageContext.request.contextPath}/user/cabinet?id=${user.getId()}" method="post">
    <input type="submit" value="${bundle.getString("user.addBook")}">
</form>
<form action="${pageContext.request.contextPath}" method="post">

    <input type="submit" value="${bundle.getString("homePage")}">
</form>
</body>
</html>
