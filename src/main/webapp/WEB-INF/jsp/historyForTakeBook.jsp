<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Available Book</title>
    <style>
        table {
            width: 85%;
            border-collapse: collapse;
        }

        th {
            background: #afd792;
            color: #333;
        }
        table td:hover {
            background: lightgray;
        }

        body tr:hover {
            background: grey;
            color: #fff;
        }
    </style>
</head>
<body>
<table border="1">
    <tr>
        <th>Id</th>
        <th>${bundleDomain.getString("book.name")}</th>
        <th>${bundleDomain.getString("book.countOfSheets")}</th>
        <th>${bundleDomain.getString("book.yearOfWrite")}</th>
        <th>${bundleDomain.getString("book.authorId")}</th>
        <th>${bundle.getString("action")}</th>
    </tr>
    <c:forEach var="book" items="${books}">
        <tr>
            <td><c:out value="${book.getId()}"></c:out></td>
            <td><c:out value="${book.getNameOfBook()}"></c:out></td>
            <td><c:out value="${book.getCountOfSheets()}"></c:out></td>
            <td><c:out value="${book.getYearOfWrite()}"></c:out></td>
            <td><c:out value="${book.getAuthor().getId()}"></c:out></td>
            <td><form action="${pageContext.request.contextPath}/take?id=${id}&book=${book.getId()}" method="post">
                <input type="submit" value="${bundle.getString("take")}">
            </form></td>
        </tr>
    </c:forEach>
</table>
<form action="${pageContext.request.contextPath}" method="post">

    <input type="submit" value="${bundle.getString("homePage")}">
</form>
</body>
</html>
