<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Success return</title>
</head>
<body>
<h1>${bundle.getString("congratulate")}</h1>
<b>${message}</b>
<form action="${pageContext.request.contextPath}" method="post">

    <input type="submit" value="${bundle.getString("homePage")}">
</form>
</body>
</html>
